<?php
session_start();

// On inclus notre fichier system
require '../app/config/system.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title><?= SITE_NAME; ?></title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <main role="main" class="container">
      <h1 class="mt-5"><a href="index.php"><?= SITE_NAME; ?></a></h1>
	    <span>Bienvenue<a href="#"> <?= $_SESSION['user_nom'].' '.$_SESSION['user_prenom']; ?></a></span>
      <br><a href="logout.php">Se deconnecter</a>

      <hr>
      <!--  Si il y a un erreur on initialise la variable message et on affiche $message -->  
      <?php if(!empty($message)): ?>
        <p><font color="red"><?= $message ?></font></p>
      <?php endif; ?>
      
      <div class="article-form">
        <?php 
          // On inclus notre fichier system
          require '../models/confirm.php';
        ?>
      </div>
    </main>
  </body>

</html>