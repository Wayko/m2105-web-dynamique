<?php

session_start();
// On inclus notre fichier system
require '../app/config/system.php';
// On inclus le fichier model
require '../models/login.php';

?>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title><?= SITE_NAME; ?></title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <main role="main" class="container">
      <h1 class="mt-5"><a href="index.php"><?= SITE_NAME; ?></a></h1>
      <h1>s'identifier</h1>
	  <span>ou <a href="register.php">Inscrivez-vous ici</a></span>
      <hr>
      <!--  Si il y a un erreur on initialise la variable message et on affiche $message -->  	
      <?php if(!empty($message)): ?>
        <p><font color="red"><?= $message ?></font></p>
      <?php endif; ?>
      <div class="article-form">
          <form method="POST" action="login.php">
              <div class="form-group">
                  <label for="id">Identifiant</label>
                  <input class="form-control" type="number" name="id" placeholder="identifiant">
              </div>
              
              <div class="form-group">
                  <label for="password">Mot de passe</label>
                  <input class="form-control" type="password" name="password" placeholder="mot de passe">
              </div>
              
              <div class="form-group">
                  <input type="submit" class="btn btn-success" value="SE CONNECTER" name="submit">
              </div>
          </form>
      </div>
    </main>
  </body>

</html>
