<?php
session_start();

// On inclus notre fichier system
require '../app/config/system.php';
// On inclus le fichier model
require '../models/register.php';
?>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
	    <title><?= SITE_NAME; ?></title>
	    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	</head>

	<body>
	    <main role="main" class="container">
		 	<h1 class="mt-5"><a href="index.php"><?= SITE_NAME; ?></a></h1>

			<h1>Inscrivez vous</h1>
			<span>ou <a href="login.php">Connectez vous!</a></span>
		 	<hr>
		 	<!--  Si il y a un erreur on initialise la variable message et on affiche $message --> 
		 	<?php if(!empty($message)): ?>
				<p><font color="red"><?= $message ?></font></p>
			<?php endif; ?>
	    	<div class="articles-list">
				<form action="register.php" method="POST">
					<input type="number" class="form-control" placeholder="Identifiant etudiant" name="id"><br>
					<input type="text" class="form-control" placeholder="Nom" name="nom"><br>
					<input type="text" class="form-control" placeholder="Prenom" name="prenom"><br>
					<input type="text" class="form-control" placeholder="Email" name="email"><br>
					<input type="password" class="form-control" placeholder="Mot de passe" name="password"><br>
					<input type="password" class="form-control" placeholder="Confirmer votre mot de passe" name="confirm_password"><br>
					<input type="submit" class="btn btn-success" name="submit">
				</form>
	      	</div>
	    </main>
	</body>
</html>