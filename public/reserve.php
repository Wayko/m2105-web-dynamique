<?php

session_start();

// On inclus notre fichier system
require '../app/config/system.php';
// On inclus notre fichier system
require '../models/reserve.php';

?>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title><?= SITE_NAME; ?></title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <main role="main" class="container">
      <h1 class="mt-5"><a href="index.php"><?= SITE_NAME; ?></a></h1>
	    <span>Bienvenue<a href="#"> <?= $_SESSION['user_nom'].' '.$_SESSION['user_prenom']; ?></a></span>
      <br><a href="logout.php">Se deconnecter</a>
      
      <hr>

      <div class="article-form">
        <!--  Si il y a un erreur on initialise la variable message et on affiche $message -->    
        <?php if(!empty($message)): ?>
        <p><font color="red"><?= $message ?></font></p>
        <?php endif; ?>
        <?="<label>Vous êtes sur le points de commander un <b>$type $name ($id)</b></label>." ?>
        <br><br>
        <p>Veuillez selectionner votre date de reservation:</p>
        <form action="" method="POST">
            <input type="hidden" value="<?= $id ?>" name="id">
            <div class="form-group">
                <input class="form-control" type="date" name="date">
            </div>
            <p>Veuillez entrez votre mot de passe afin de confirmer votre reservation:</p>       
            <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="mot de passe">
            </div>
            <center>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="VALIDER" name="submit">
            </div>
            </center>
        </form>
      </div>
    </main>
  </body>

</html>