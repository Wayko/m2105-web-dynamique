<?php
	session_start();
    // On inclus notre fichier system
    require '../app/config/system.php';
    // On inclus le fichier model
  	require '../models/index.php';
?>

<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?= SITE_NAME; ?></title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <main role="main" class="container">
      <center>

        <img src="assets/img/logo_ur.jpg" width="800" height="230">
        <hr>
        <div class="articles-list">
        <!-- Condition permettant de renvoyer l'utilisateur dans le dashboard si il est déjà conneter sinon il se connecte -->
    		<?php if( !empty($results) ): header("Location: dashboard.php") ?>
    		<?php else: ?>
    			<h1>S'il vous plaît</h1>
    			<a href="login.php">connectez vous</a> ou
    			<a href="register.php">inscrivez vous</a>
    		<?php endif; ?>
        </div>
      </center>
    </main>
  </body>
</html>
