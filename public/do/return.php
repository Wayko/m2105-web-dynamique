<?php

session_start();

// On inclus notre fichier system
require '../../app/config/system.php';

//Si la session existe et que le role est utilisateur(0) 
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0)
{
  	$approve = 2;
  	$stmt = $bdd->prepare('UPDATE commande SET approve = :approve WHERE materiel = :materiel');
	$stmt->bindParam(':approve', $approve);
	$stmt->bindParam(':materiel', $_GET['id']);
	$stmt->execute();

	header("Location: ../mylist.php");
}
//Si la session existe et que le role est administrateur(1) 
elseif( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1)
{
	//On supprimer l'identifiant du materiel dans la table commande puis on change sont statut en disponible
  	$stmt = $bdd->prepare('DELETE FROM commande WHERE materiel = :materiel');
	$stmt->bindParam(':materiel', $_GET['id']);
	$stmt->execute();

	$approve = 'Disponible';
	$stmt = $bdd->prepare('UPDATE materiel SET statut = :statut WHERE id_materiel = :id_materiel');
	$stmt->bindParam(':statut', $approve);
	$stmt->bindParam(':id_materiel', $_GET['id']);
	$stmt->execute();
	
	header("Location: ../mylist.php");
}
else
{
	header("Location: ../index.php");
}
