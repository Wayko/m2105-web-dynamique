<?php

session_start();

// On inclus notre fichier system
require '../../app/config/system.php';

//Si la session existe et que le role est utilisateur(0) 
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0)
{
  	//On fait une requete preparer pour reserver un materiel qui sera mis en attentent de confirmation
  	$stmt = $bdd->prepare('INSERT INTO commande(id_user, materiel, temps) VALUES (:id_user, :materiel, :temps)');
	$stmt->bindParam(':id_user', $_SESSION['user_id']);
	$stmt->bindParam(':materiel', $_SESSION['id_materiel']);
	$stmt->bindParam(':temps', $_SESSION['date_materiel']);
	$stmt->execute();
	header("Location: ../dashboard.php");
}
//Si la session existe et que le role est administrateur(1) 
elseif( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1)
{
  	//On fait une requette preparer pour reserver directement un materiel sans attentent de confirmation
  	$approve = 1;
  	$stmt = $bdd->prepare('INSERT INTO commande(id_user, materiel, temps, approve) VALUES (:id_user, :materiel, :temps, :approve)');
	$stmt->bindParam(':id_user', $_SESSION['user_id']);
	$stmt->bindParam(':materiel', $_SESSION['id_materiel']);
	$stmt->bindParam(':temps', $_SESSION['date_materiel']);
	$stmt->bindParam(':approve', $approve);
	$stmt->execute();

	//On change le statut de l'objet emprunter en le rendant indisponible
	$approve = 'Indisponible';
	$stmt = $bdd->prepare('UPDATE materiel SET statut = :statut WHERE id_materiel = :id_materiel');
	$stmt->bindParam(':statut', $approve);
	$stmt->bindParam(':id_materiel', $_SESSION['id_materiel']);
	$stmt->execute();

	header("Location: ../dashboard.php");
}
//On rajoute cette condition pour eviter les utilisateur malveillant
else
{
	header("Location: ../index.php");
}
