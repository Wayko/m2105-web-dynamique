<?php

session_start();

// On inclus notre fichier system
require '../../app/config/system.php';

//Si la session est pas admin on redirige le client 
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0 OR !isset($_SESSION['user_id']))
{
	header("Location: ../index.php");
}
//Si la session est admin on affiche les differents commande permettant ainsi : d'approuver une commande /valider un retour / supprimer une commande 
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1)
{
	$stmt = $bdd->prepare('SELECT approve FROM commande WHERE materiel = :materiel');
	$stmt->bindParam(':materiel', $_GET['materiel']);
	$stmt->execute();
	$results = $stmt->fetch(PDO::FETCH_ASSOC);

	if ($results['approve'] == 0) 
	{
		$stmt = $bdd->prepare('UPDATE commande SET approve = :approve WHERE materiel = :materiel');
		$approve = 1;
		$stmt->bindParam(':approve', $approve);
		$stmt->bindParam(':materiel', $_GET['materiel']);
		$stmt->execute();

		$stmt = $bdd->prepare('UPDATE materiel SET statut = :statut WHERE id_materiel = :id_materiel');
		$approve = 'Indisponible';
		$stmt->bindParam(':statut', $approve);
		$stmt->bindParam(':id_materiel', $_GET['materiel']);
		$stmt->execute();
	}
	elseif ($results['approve'] == 2) 
	{
		$stmt = $bdd->prepare('DELETE FROM commande WHERE materiel = :materiel');
		$stmt->bindParam(':materiel', $_GET['materiel']);
		$stmt->execute();

		$approve = 'Disponible';
		$stmt = $bdd->prepare('UPDATE materiel SET statut = :statut WHERE id_materiel = :id_materiel');
		$stmt->bindParam(':statut', $approve);
		$stmt->bindParam(':id_materiel', $_GET['materiel']);
		$stmt->execute();
	}
	header("Location: ../confirm.php");
}

