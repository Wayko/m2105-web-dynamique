<?php

// On inclus notre fichier system
require '../../app/config/system.php';

if(!empty($_POST['id']) && !empty($_POST['password']))
{
	$records = $bdd->prepare('SELECT id_user,mpd,admin FROM user WHERE id_user = :id_user');
	$records->bindParam(':id_user', $_POST['id']);
	$records->execute();
	$results = $records->fetch(PDO::FETCH_ASSOC);

	$message = '';

	if(count($results) > 0 && password_verify($_POST['password'], $results['mpd']) ){

		$_SESSION['user_id'] = $results['id_user'];
		header("Location: ../public/dashboard.php");

	} else {
		$message = 'Sorry, those credentials do not match';
	}
}