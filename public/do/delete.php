<?php

session_start();

// On inclus notre fichier system
require '../../app/config/system.php';
//Si la session est pas admin on redirige le client 
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0 OR !isset($_SESSION['user_id']))
{
  header("Location: index.php");
}
//Si la session est admin on recupere l'identifiant du materiel a suprimmer dans la table materiel et commande 
if ( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1 ) 
{
	if (isset($_GET['id'])) 
	{
		$stmt = $bdd->prepare('DELETE FROM materiel WHERE id_materiel = :id_materiel');
		$stmt->bindParam(':id_materiel', $_GET['id']);
		$stmt->execute();
		header("Location: ../dashboard.php");
	}
	
	elseif (isset($_GET['materiel'])) 
	{
		$stmt = $bdd->prepare('DELETE FROM commande WHERE materiel = :materiel');
		$stmt->bindParam(':materiel', $_GET['materiel']);
		$stmt->execute();
		header("Location: ../confirm.php");
	}
}
