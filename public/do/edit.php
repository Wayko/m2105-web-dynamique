<?php
session_start();

// On inclus notre fichier system
require '../../app/config/system.php';
//Si la session est pas admin on redirige le client 
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0 OR !isset($_SESSION['user_id']))
{
	header("Location: ../index.php");
}
//Si l'utilisateur est admin on update la table materiel avec les variables session defini précédement
elseif( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1)
{
	$stmt = $bdd->prepare('UPDATE materiel SET type= :type ,nom= :nom ,statut= :statut WHERE id_materiel = :id');
	$stmt->bindParam(':type', $_SESSION['m_type_materiel']);
	$stmt->bindParam(':nom', $_SESSION['m_nom_materiel']);
	$stmt->bindParam(':statut', $_SESSION['m_statut_materiel']);
	$stmt->bindParam(':id', $_SESSION['m_id_materiel']);
	$stmt->execute();

	header("Location: ../dashboard.php");
}

