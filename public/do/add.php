<?php

session_start();

// On inclus notre fichier system
require '../../app/config/system.php';
//Si la session n'existe pas ou quels existe et que le role utilisateur = 0 on retourne l'utilisateur vers l'index
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0 OR !isset($_SESSION['user_id']))
{
	header("Location: ../index.php");
}
//Si la session existe et que le role est administrateur(1)
elseif( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1)
{
	//On fait une requete preparer qui vas inserer un nouveau materiel dans la table materiel
	$stmt = $bdd->prepare('INSERT INTO materiel (type, nom, statut) VALUES (:type, :nom, :statut)');
	$stmt->bindParam(':type', $_SESSION['a_type_materiel']);
	$stmt->bindParam(':nom', $_SESSION['a_nom_materiel']);
	$stmt->bindParam(':statut', $_SESSION['a_statut_materiel']);
	$stmt->execute();
	//On retourne sur le dashboard
	header("Location: ../dashboard.php");
}

