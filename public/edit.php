<?php
session_start();

// On inclus notre fichier system
require '../app/config/system.php';
// On inclus notre fichier system
require '../models/edit.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title><?= SITE_NAME; ?></title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <main role="main" class="container">
      <h1 class="mt-5"><a href="index.php"><?= SITE_NAME; ?></a></h1>
	    <span>Bienvenue<a href="#"> <?= $_SESSION['user_nom'].' '.$_SESSION['user_prenom']; ?></a></span>
      <br><a href="logout.php">Se deconnecter</a>

      <hr>
      <!--  Si il y a un erreur on initialise la variable message et on affiche $message -->  
      <?php if(!empty($message)): ?>
        <p><font color="red"><?= $message ?></font></p>
      <?php endif; ?>

      <div class="article-form">
        <?="<label>Vous êtes sur le points de modifier <b>$type $name ($id)</b></label>." ?>
        <br><br>
        <p>Veuillez completez les champs ci-dessous:</p>
        <form action="" method="POST">

              <div class="form-group">   
                <label>Type:</label> 
                <select class="form-control" name="type">
                  <option value="Switch">Switch</option>
                  <option value="PC">PC</option>
                  <option value="Pièces">Pièces</option>
                  <option value="Objets">Objets</option>
                </select>
              </div>
              
              <div class="form-group">
                  <label>Nom:</label> 
                  <input class="form-control" type="text" name="nom" placeholder="nom du materiel">
              </div>

              <div class="form-group">
                <label>Statut:</label>
                <select class="form-control" name="statut" style="width: 12%;">
                  <option value="Disponible">Disponible</option>
                  <option value="Indisponible">Indisponible</option>
                  <option value="En reparation">En reparation</option>
                </select>
              </div>

              <center>
              <div class="form-group">
                  <input type="submit" class="btn btn-success" value="VALIDER" name="submit">
              </div>
              </center>

        </form>
      </div>
    </main>
  </body>

</html>