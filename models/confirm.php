<?php
//SESSION UTILISATEUR
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0 OR !isset($_SESSION['user_id']))
{
  header("Location: index.php");
}

$stmt = $bdd->query('SELECT COUNT(*)  FROM commande');
$count_all = $stmt->fetchColumn();

if ($count_all > 0) 
{
	//SESSION ADMINISTRATEUR
	if (isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1) 
	{

		$stmt = $bdd->query('SELECT COUNT(approve)  FROM commande WHERE approve = 1');
		$count_material_1 = $stmt->fetchColumn(); //On compte le nombre de materiel approuver

		echo "<label style=\"float: right;\">";
		echo "<b>Approuvé</b>";
		echo "</label>";

		//DEBUT TABLEAU MATERIEL APPROUVE
		echo "<form>";
			echo "<table class=\"table\">";
				echo "<thead class=\"thead-light\">";
					echo "<tr>";
						echo "<th scope=\"col\">IDENTIFIANT</th>";
						echo "<th scope=\"col\">MATERIEL</th>";
						echo "<th scope=\"col\">DATE</th>";
					echo "</tr>";
				echo "</thead>";
				echo "<tbody>";

		if ($count_material_1 > 0) 
		{
			$material_1 = $bdd->query('SELECT id_user,materiel,temps,approve FROM commande WHERE approve = 1');
			while ($donnees = $material_1->fetch())// On affiche chaque entrée une à une
			{
				$id = $donnees['id_user'];
				$materiel = $donnees['materiel'];
				$temps = $donnees['temps'];

				echo"<tr>
						<th scope=\"row\">"
							.$id.
						"</th>
						<td>"
							.$materiel.
						"</td>
						<td>"
							.$temps.
						"</td>
					</tr>";
			}
			$material_1->closeCursor(); // Termine le traitement de la requête
		}
		else
		{
			echo '<tr><td>Aucune demande approuvé</td></tr>';
		}
		echo '</tbody></table></form>';//FIN TABLEAU MATERIEL APPROUVE
		
		$stmt = $bdd->query('SELECT COUNT(approve)  FROM commande WHERE approve = 0');
		$count_material_0 = $stmt->fetchColumn(); //On compte le nombre de materiel non approuver

		echo "<hr>";
		echo "<br>";
		echo "<label style=\"float: right;\">";
		echo "<b>En attentent de validation</b>";
		echo "</label>";

		//DEBUT TABLEAU MATERIEL EN ATTENTENT DE VALIDATION
		echo "<form>";
			echo "<table class=\"table\">";
				echo "<thead class=\"thead-light\">";
					echo "<tr>";
						echo "<th scope=\"col\">IDENTIFIANT</th>";
						echo "<th scope=\"col\">MATERIEL</th>";
						echo "<th scope=\"col\">DATE</th>";
						echo "<th scope=\"col\">APPROUVER</th>";
						echo "<th scope=\"col\">SUPPRIMER</th>";
					echo "</tr>";
				echo "</thead>";
				echo "<tbody>";

		if ($count_material_0 > 0) 
		{
			$material_0 = $bdd->query('SELECT id_user,materiel,temps,approve FROM commande WHERE approve = 0');
			while ($donnees = $material_0->fetch())// On affiche chaque entrée une à une
			{	
				$id = $donnees['id_user'];
				$materiel = $donnees['materiel'];
				$temps = $donnees['temps'];

				echo"<tr>
						<th scope=\"row\">"
							.$id.
						"</th>
						<td>"
							.$materiel.
						"</td>
						<td>"
							.$temps.
						"</td>
						<td>"
							."<a href=\"do/confirm.php?materiel=$materiel\" class=\"btn btn-success\">valider</a>".
						"</td>
						<td>"
							."<a href=\"do/delete.php?materiel=$materiel\" class=\"btn btn-danger\">supprimer</a>".
						"</td>
					</tr>";
			}
			$material_0->closeCursor(); // Termine le traitement de la requête
		}
		else
		{
			echo '<tr><td>Aucune demande en attentent de validation</td></tr>';
		}
		echo "</tbody>";
		echo "</table>";
		echo "</form>";
		//FIN TABLEAU MATERIEL EN ATTENTENT DE VALIDATION

		echo "<hr>";
		echo "<br>";
		echo "<label style=\"float: right;\">";
		echo "<b>Retour<b>";
		echo "</label>";

		//DEBUT TABLEAU RETOUR MATERIEL
		echo "<form>";
			echo "<table class=\"table\">";
				echo "<thead class=\"thead-light\">";
					echo "<tr>";
						echo "<th scope=\"col\">IDENTIFIANT</th>";
						echo "<th scope=\"col\">MATERIEL</th>";
						echo "<th scope=\"col\">DATE</th>";
						echo "<th scope=\"col\">RETOUR</th>";
					echo "</tr>";
				echo "</thead>";
				echo "<tbody>";

		
		$stmt = $bdd->query('SELECT COUNT(approve)  FROM commande WHERE approve = 2');
		$count_material_2 = $stmt->fetchColumn();//On compte le nombre de materiel demande de retour

		if ($count_material_2 > 0) 
		{
			$material_2 = $bdd->query('SELECT id_user,materiel,temps,approve FROM commande WHERE approve = 2');
			while ($donnees = $material_2->fetch())// On affiche chaque entrée une à une
			{
					
				$id = $donnees['id_user'];
				$materiel = $donnees['materiel'];
				$temps = $donnees['temps'];

				echo"<tr>
						<th scope=\"row\">"
							.$id.
						"</th>
						<td>"
							.$materiel.
						"</td>
						<td>"
							.$temps.
						"</td>
						<td>"
							."<a href=\"do/confirm.php?materiel=$materiel\" class=\"btn btn-primary\">valider</a>".
						"</td>
					</tr>";
			}
			$material_2->closeCursor(); // Termine le traitement de la requête
			echo '</tbody></table></form>';//FIN TABLEAU RETOUR MATERIEL
		}
		else
		{
			echo "<tr><td>Aucune demande de retour</td></tr>";        
		}
	}	
}
else
{
	echo 'Aucune commande n\'est en attentent de gestion. Pour retourner à la page d\'accueil <a href="dashboard.php">cliquez ici !</a></p>';
}

