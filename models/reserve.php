<?php
//On stock les différentes informations $_GET dans des variables 
$id = $_GET['id'];
$type = $_GET['type'];
$name = $_GET['name'];
$statut = $_GET['statut'];
//On initialise le $message à zero pour ne rien afficher 
$message =''; 

//Si on clique sur le bouton "submit"
if ( isset($_POST['submit']) )
{
	//On verifie que les champs "date" et "password" ne sont pas vide
	if( !empty($_POST['date']) && !empty($_POST['password']) )
	{
		//A l'aide de la fonction password_verify on verifie que le mot de passe entré est le même que le mot de passe de session
		if( password_verify($_POST['password'], $_SESSION['user_password']))
		{
			//On créer des variables session qu'on vas utiliser plus tard (do/reserve.php)
			$_SESSION['id_materiel'] = $id;
			$_SESSION['date_materiel'] = $_POST['date'];
			//On se dirige vers do/reserve.php
			header("Location: do/reserve.php");
		} 
		//Sinon on initialise la variable $message pour afficher un code d'erreur dans le reserve.php
		else 
		{
			$message = 'Désolé, ces informations d\'identification ne correspondent pas';
		}
	}
}
