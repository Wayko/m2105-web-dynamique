<?php
//Si l'utilisateur est déjà connecter on le renvoie vers le dashboard
if( isset($_SESSION['user_id']) )
{
	header("Location: dashboard.php");
}
//Si les champs identifiant et mot de passe sont rempli:
if(!empty($_POST['id']) && !empty($_POST['password'])):
	
	$records = $bdd->prepare('SELECT id,admin,nom,prenom,email,password FROM users WHERE id = :id');
	$records->bindParam(':id', $_POST['id']);
	$records->execute();
	$results = $records->fetch(PDO::FETCH_ASSOC);

	$message = '';
	//On verifie que l'utilisateur existe dans la base de donnée puis on compare les mot de passe 
	if(count($results) > 0 && password_verify($_POST['password'], $results['password']) )
	{
		//Si c'est vrai on créer des variables sessions puis on le redirige vers le dashboard
		$_SESSION['user_id'] = $results['id'];
		$_SESSION['user_password'] = $results['password'];
		$_SESSION['user_admin'] = $results['admin'];
		$_SESSION['user_nom'] = $results['nom'];
		$_SESSION['user_prenom'] = $results['prenom'];
		//On se dirige vers dashboard.php
		header("Location: dashboard.php");

	} 
	//Sinon on initialise la variable message qui apparait dans le login.php
	else 
	{
		$message = 'Désolé, ces informations d\'identification ne correspondent pas';
	}

endif;
