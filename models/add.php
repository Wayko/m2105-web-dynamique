<?php
//Si la session est utilisateur ou elle existe pas on renvoie vers index.php
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0 OR !isset($_SESSION['user_id']))
{
  header("Location: index.php");
}
//Si la session existe et detient le role administrateur 
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1)
{

	$message =''; 
	//Si on clique sur le bouton "submit"
	if ( isset($_POST['submit']) ):
		//Si tout les champs type, nom, statut sont rempli
		if( !empty($_POST['type']) && !empty($_POST['nom']) && !empty($_POST['statut']) ){

				//On créer des variables session qu'on vas utiliser plus tard (do/add.php)
				$_SESSION['a_type_materiel'] = $_POST['type'];
				$_SESSION['a_nom_materiel'] = $_POST['nom'];
				$_SESSION['a_statut_materiel'] = $_POST['statut'];
				//On se dirige vers do/reserve.php
				header("Location: do/add.php");
		}
		//Sinon on initialise la variable $message pour afficher un code d'erreur dans le reserve.php
		else
		{
				$message = 'Veuillez compléter tous les champs !';
		}
	endif;
}



