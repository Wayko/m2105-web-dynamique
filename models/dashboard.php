<?php
//En fonction de la SESSION['user_admin'] on affiche un dashboard différent:

//DASHBOARD ADMIN 
if (isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1) 
{
	$material = $bdd->query('SELECT id_materiel,nom,type,statut FROM materiel');

	echo "<center>| ";
		echo "<a href=\"confirm.php\">Gestionnaire de commande</a> | ";
		echo "<a href=\"mylist.php\">Mes reservations</a> | ";
		echo "<a href=\"add.php\">Ajouter un article</a> |";
	echo "</center>";
	
	echo "<br>";

	echo "<form>";
	echo "<table class=\"table\">";
		echo "<thead class=\"thead-light\">";
			echo "<tr>";
				echo "<th scope=\"col\">ID</th>";
				echo "<th scope=\"col\">TYPE</th>";
				echo "<th scope=\"col\">NOM</th>";
				echo "<th scope=\"col\">STATUT</th>";
				echo "<th scope=\"col\">RESERVER</th>";
				echo "<th scope=\"col\">MODIFIER</th>";
				echo "<th scope=\"col\">SUPPRIMER</th>";
			echo "</tr>";
		echo "</thead>";
		echo "<tbody>";

	while ($donnees = $material->fetch())// On affiche chaque entrée une à une
	{
		$id = $donnees['id_materiel'];
		$type = $donnees['type'];
		$name = $donnees['nom'];
		$status = $donnees['statut'];

		echo"<tr>
				<th scope=\"row\">"
					.$id.
				"</th>
				<td>"
					.$type.
				"</td>
				<td>"
					.$name.
				"</td>
				<td>"
					.$status.
				"</td>
				<td>"
					."<a href=\"reserve.php?id=$id&type=$type&name=$name&statut=$status\" class=\"btn btn-primary\">reserver</a>".
				"</td>
				<td>"
					."<a href=\"edit.php?id=$id&type=$type&name=$name&statut=$status\" class=\"btn btn-success\">modifier</a>".
				"</td>
				<td>"
					."<a href=\"do/delete.php?id=$id\" class=\"btn btn-danger\">supprimer</a>".
				"</td>
			</tr>";
	}
	$material->closeCursor(); // Termine le traitement de la requête

			echo "</tbody>";
		echo "</table>";
	echo "</form>";
}

//DASHBOARD UTILISATEUR
elseif (isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0) 
{
	$material_d = $bdd->query('SELECT id_materiel,nom,type,statut FROM materiel WHERE statut = \'Disponible\'');
	
	echo "<center>| ";
	echo "<a href=\"mylist.php\">Mes reservations</a>  |";
	echo "</center>";

	echo "<br>";

	//DEBUT TABLEAU MATERIEL DISPONIBLE
	echo "<table class=\"table\">";
		echo "<thead class=\"thead-light\">";
			echo "<tr>";
				echo "<th scope=\"col\">ID</th>";
				echo "<th scope=\"col\">TYPE</th>";
				echo "<th scope=\"col\">NOM</th>";
				echo "<th scope=\"col\">RESERVER</th>";
			echo "</tr>";
		echo "</thead>";
		echo "<tbody>";

	while ($donnees = $material_d->fetch())// On affiche chaque entrée une à une
	{
		$id = $donnees['id_materiel'];
		$type = $donnees['type'];
		$name = $donnees['nom'];
		$status = $donnees['statut'];

		echo"<tr>
				<th scope=\"row\">"
					.$id.
				"</th>
				<td>"
					.$type.
				"</td>
				<td>"
					.$name.
				"</td>
				<td>"
					."<a href=\"reserve.php?id=$id&type=$type&name=$name&statut=$status\" class=\"btn btn-primary\">reserver</a>".
				"</td>
			</tr>";
	}
	$material_d->closeCursor(); // Termine le traitement de la requête
	//FIN TABLEAU MATERIEL DISPONIBLE
	echo "</tbody>";
	echo "</table>";
}
//On revoit les utilisateurs malveillant sur la page d'accueil
else
{
	header("Location: index.php");
}



