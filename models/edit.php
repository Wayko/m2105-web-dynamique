<?php
//Si la session est utilisateur ou elle existe pas on renvoie vers index.php
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0 OR !isset($_SESSION['user_id']))
{
  header("Location: index.php");
}
//Si la session existe et detient le role administrateur 
if( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1)
{
	//On stock les différents informations GET dans des variables
	$id = $_GET['id'];
	$type = $_GET['type'];
	$name = $_GET['name'];
	$statut = $_GET['statut'];
	//Initialisation de la variable message 
	$message =''; 
	//Si le boutton a été cliquée 
	if ( isset($_POST['submit']) ):
		//Si les différents champs sont remplie 
		if( !empty($_POST['type']) && !empty($_POST['nom']) && !empty($_POST['statut']) ){
				//On créer des variables sessions qu'on vas utiliser ensuite (do/edit.php)
				$_SESSION['m_type_materiel'] = $_POST['type'];
				$_SESSION['m_nom_materiel'] = $_POST['nom'];
				$_SESSION['m_statut_materiel'] = $_POST['statut'];
				$_SESSION['m_id_materiel'] = $id;
				header("Location: do/edit.php");
		}
		//Sinon on initialise la variable message avec un message d'erreur 
		else
		{
				$message = 'Désolé, ces informations d\'identification ne correspondent pas';
		}
	endif;
}



