<?php
//On initialise la variable message 
$message = '';
//Si on clique sur le button "submit" 
if ( isset($_POST['submit']) ):
	//Si tout les champs sont remplie
	if(!empty($_POST['id']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['email']) && !empty($_POST['password'])):
		
		// A l'aide d'une requete preparer on entre un nouvelle utilisateur dans la bdd
		$stmt = $bdd->prepare('INSERT INTO users (id, nom, prenom, email, password) VALUES (:id, :nom, :prenom, :email, :password)');

		$stmt->bindParam(':id', $_POST['id']);
		$stmt->bindParam(':nom', $_POST['nom']);
		$stmt->bindParam(':prenom', $_POST['prenom']);
		$stmt->bindParam(':email', $_POST['email']);
		$stmt->bindParam(':password', password_hash($_POST['password'], PASSWORD_BCRYPT));

		//Quand on execute on retourne l'utilisateur au dashboard
		if( $stmt->execute() ):
			header("Location: dashboard.php");
		endif;
	else:
		$message = 'Veuillez completez tout les champs s\'il vous plaît';
	endif;
endif;