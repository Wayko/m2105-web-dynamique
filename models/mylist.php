<?php
//Si une session existe
if ( isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 0 OR isset($_SESSION['user_id']) && $_SESSION['user_admin'] == 1) 
{
	//Requete permettant de compter le nombre de materiel de la session connecter ($count-materiel_user)
	$stmt = $bdd->prepare('SELECT COUNT(materiel)  FROM commande WHERE id_user = :id_user');
	$stmt->bindParam(':id_user', $_SESSION['user_id']);
	$stmt->execute();
	$count_materiel_user = $stmt->fetchColumn();

	//Requete permmetant de savoir si la commande est apprové (1) ou non (0) 
	$rqt = $bdd->prepare('SELECT approve FROM commande WHERE id_user = :id_user');
	$rqt->bindParam(':id_user', $_SESSION['user_id']);
	$rqt->execute();
	$results = $rqt->fetch(PDO::FETCH_ASSOC);

	//Si le nombre de materiaux est supérieur a 0 et qu'il a était aprouvé 
	if ($count_materiel_user > 0 AND $results['approve'] == 1) 
	{
		//ON AFFICHE LES MATERIAUX VALIDER 
		echo "<form>";
			echo "<table class=\"table\">";
				echo "<thead class=\"thead-light\">";
					echo "<tr>";
						echo "<th scope=\"col\">ID</th>";
						echo "<th scope=\"col\">TYPE</th>";
						echo "<th scope=\"col\">NOM</th>";
						echo "<th scope=\"col\">DATE</th>";
						echo "<th scope=\"col\">RENDRE</th>";
					echo "</tr>";
				echo "</thead>";
				echo "<tbody>";


		$approve = 1;
		$stmt = $bdd->prepare('SELECT materiel,temps FROM commande WHERE approve = :approve AND id_user = :id_user');
		$stmt->bindParam(':approve', $approve);
		$stmt->bindParam(':id_user', $_SESSION['user_id']);
		$stmt->execute();

		while ($donnees = $stmt->fetch())// On affiche chaque entrée une à une
		{
			$materiel = $donnees['materiel'];

			$rqt = $bdd->prepare('SELECT id_materiel,type,nom FROM materiel WHERE id_materiel = :id_materiel');
			$rqt->bindParam(':id_materiel', $donnees['materiel']);
			$rqt->execute();
			$results = $rqt->fetch(PDO::FETCH_ASSOC);

			echo"<tr>
					<th scope=\"row\">"
						.$results['id_materiel'].
					"</th>
					<td>"
						.$results['type'].
					"</td>
					<td>"
						.$results['nom'].
					"</td>
					<td>"
						.$donnees['temps'].
					"</td>
					<td>"
						."<a href=\"do/return.php?id=$materiel\" class=\"btn btn-primary\">valider</a>".
					"</td>
				</tr>";
		}
		$stmt->closeCursor(); // Termine le traitement de la requête
		echo '</tbody></table></form>';
	}
	//Sinon on affiche un message pour dire que aucun matériel n'a été réserver ou approuvé 
	else
	{
		echo "<p>Vous n'avez actuellement réservé aucun matériel. Pour réserver, <a href=\"dashboard.php\">cliquez-ici !</a><p>";
	}
	
}
//Ajouter pour eviter les utilisateurs malveillants
else
{
	header("Location: index.php");
}